import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { UserModel } from '../models/user.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  users: UserModel[];

  constructor( private db: AngularFirestore) { }

  ngOnInit() {
    this.db.collection('Users').valueChanges().subscribe((users: UserModel[]) => {
      this.users = users;
      console.log(this.users)
    })
  }

}
